import { Album, AlbumSearchResponse, FullAlbum } from 'src/app/core/model/search';
import { Inject, Injectable } from '@angular/core';

import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { API_URL, INITIAL_RESULTS } from '../../tokens';
import { AuthService } from '../auth/auth.service';
import { catchError, map, Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class MusicAPIService {

  constructor(
    private http: HttpClient,
  ) { }

  fetchAlbumById(id: string): Observable<FullAlbum> {
    return this.http.get<FullAlbum>(`/albums/` + id, {})
  }

  fetchSearchResults(query = 'batman'): Observable<Album[]> {
    return this.http.get<AlbumSearchResponse>(`/search`, {
      params: { type: 'album', q: query },
    }).pipe(
      map(resp => resp.albums.items),
    )
  }
}