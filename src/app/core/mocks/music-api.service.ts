import { Injectable } from '@angular/core';
import { mockAlbums } from './mockAlbums';


@Injectable({
  providedIn: 'root',
})
export class MockMusicAPIService {

  constructor() { }

  fetchSearchResults(query = 'batman') {
    return mockAlbums
  }
}
