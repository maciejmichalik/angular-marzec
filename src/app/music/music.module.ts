import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicRoutingModule } from './music-routing.module';
import { MusicComponent } from './music.component';
import { AlbumSearchContainer } from './containers/album-search/album-search.container';
import { AlbumDetailsContainer } from './containers/album-details/album-details.container';
import { SearchFormComponent } from './components/search-form/search-form.component';
import { ResultsGridComponent } from './components/results-grid/results-grid.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    MusicComponent,
    AlbumSearchContainer,
    AlbumDetailsContainer,
    SearchFormComponent,
    ResultsGridComponent,
    AlbumCardComponent
  ],
  imports: [
    CommonModule,
    MusicRoutingModule,
    SharedModule
  ],
  exports: []
})
export class MusicModule { }
