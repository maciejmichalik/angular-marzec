import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlbumDetailsContainer } from './album-details.container';

describe('AlbumDetailsContainer', () => {
  let component: AlbumDetailsContainer;
  let fixture: ComponentFixture<AlbumDetailsContainer>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlbumDetailsContainer ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlbumDetailsContainer);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
